const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('express_mysql_crud', 'root', 'root123', {
  host: 'localhost',
  dialect: 'mysql',
});

module.exports = sequelize;
